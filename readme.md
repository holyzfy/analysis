# 天创统计

## 目录结构 

* `www` 项目根目录
* `tools` AMD构建脚本

请在server里预览 [http://localhost:8088/analysis/www/](http://localhost:8088/analysis/www/)

## 单元测试

[http://localhost:8088/analysis/www/test/unit.html](http://localhost:8088/analysis/www/test/unit.html)

## 功能测试

0. 安装firefox插件[Selenium IDE](http://docs.seleniumhq.org/projects/ide/)
0. 打开Selenium IDE，文件 > open 选择`selenium/test_suite.html`，点击按钮**Play entire test suite**

## 开发

* 请在本地dev分支上开发，完成一个功能后合并到master
* 只推送master分支
* 为了减小冲突，请及时把master分支合并到dev
* 其他请看[wiki](https://bitbucket.org/holyzfy/analysis/wiki/browse/)

