{
	appDir: '../www',
	mainConfigFile: '../www/js/config.js',
	dir: '../build',
	inlineText: true,
	optimize: 'none',
	modules: [
		{
			name: 'common',
			include: [
				'jquery',
				'template',
				'text',
				'common',
				'daterangepicker',
				'bootstrap-select',
				'bootstrap-switch'
			]
		},
		{
			name: 'conversion_overview',
			exclude: ['common']
		}
	]
}