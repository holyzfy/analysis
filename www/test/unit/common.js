define(function(require){
    var Qunit = require('qunit');
    var common = require('common');
    
    Qunit.test('fail', function(assert) {
        var fail = common._debug.fail;

        var err1 = {
            status: 404,
            responseText: 'Cannot GET /a_test_not_existed_url'
        };
        assert.equal(fail(err1), 'Cannot GET /a_test_not_existed_url');

        var err2 = new TypeError('this is a TypeError');
        assert.equal(fail(err2), 'this is a TypeError');

        var err3 = '测试一条出错信息';
        assert.equal(fail(err3), err3);
    });

    Qunit.test('get', function(assert) {
        assert.expect(3);
        var done = assert.async();

        var d1 = common.get('/a_not_existed_url').fail(function(err) {
            assert.ok(err);
        });

        var d2 = common.get('../mock/success.json').done(function(data) {
            var expected = {
                "status": 1,
                "message": "请求成功",
                "data": {
                    "title": "hello world"
                }
            };
            assert.deepEqual(data, expected);
        });

        var d3 = common.get('../mock/fail.json').done(function(data) {
            var expected = {
                "status": 0,
                "message": "请输入用户名"
            };
            assert.deepEqual(data, expected);
        });

        var all = [d1, d2, d3];
        $.when(all, done);

    });
});