define(function(require){
    var Qunit = require('qunit');
    var urlMap = require('urlmap');
    
    Qunit.test('getEnv', function(assert) {
        assert.equal(urlMap['/trans/transform'], 'mock/trans_transform.json');
    });

});