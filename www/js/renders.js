define(function() {

	var rend_head = function(data) {
		var header = "";
		header += "		<div class=\"row item-hd\">";
		header += "			<div class=\"col-md-3\">目标名称<\/div>";
		header += "			<div class=\"col-md-4\">目标达成位置<\/div>";
		if (data.fdimenName.length != 0) {
			header += ('<div class=\"col-md-3\">' + data.fdimenName + '<\/div>');
		}
		if (data.sdimenName.length != 0) {
			header += ('<div class=\"col-md-3\">' + data.sdimenName + '<\/div>');
		}
		header += "			<div class=\"col-md-1\">转化次数<\/div>";
		header += "			<div class=\"col-md-1\">转化率<\/div>";
		header += "			<div class=\"col-md-1\">转化价值<\/div>";
		header += "			<div class=\"col-md-1\">放弃率<\/div>";
		header += "		<\/div>";
		return header;
	};

	var rend_values = function(data) {
		var values = "";
		values += "				<div class=\"col-md-1\">" + data.transCount + "<\/div>";
		values += "				<div class=\"col-md-1\">" + data.completePercent
				+ "<\/div>";
		values += "				<div class=\"col-md-1\">" + data.transValue + "<\/div>";
		values += "				<div class=\"col-md-1\">" + data.giveupPercent
				+ "<\/div>";
		return values;
	};

	var rend_unexpanded = function(data) {
		var unexpanded = "";
		unexpanded += "			<div class=\"row\">";
		unexpanded += "				<div class=\"col-md-2\">" + data.transTargetName
				+ "<\/div>";
		unexpanded += "				<div class=\"col-md-2 item-step-bd\">";
		unexpanded += "					<div class=\"item-url\">" + data.transTargetUrl
				+ "<\/div>";
		unexpanded += "				<\/div>";
		unexpanded += "				<div class=\"col-md-3 text-right\">";
		unexpanded += "					<input type=\"checkbox\" class=\"conversion-switch\">";
		unexpanded += "				<\/div>";
		unexpanded += rend_values(data);
		unexpanded += "		   <\/div>";
		return unexpanded;

	};
	var rend_path = function(data) {
		var pathrow = "";
		pathrow += "				<div class=\"row\">";
		pathrow += "					<div class=\"col-md-3 col-md-offset-5 conversion-step-path\">路径名称："
				+ data.fDimenName + "<\/div>";
		pathrow += "				<\/div>";
		return pathrow;
	};
	var rend_step = function(data) {
		var steprow = "";
		steprow += "<div class=\"row\">";
		steprow += "					<div class=\"col-md-3 col-md-offset-5 conversion-step-item\">";
		steprow += "						步骤：" + data.fdimenName;
		steprow += "					<\/div>";
		steprow += rend_values(data);
		steprow += "<\/div>";
		return steprow;
	};
	var rend_compare_condition = function(data) {
		var comparerow = "";
		comparerow += "<div class=\"row\">";
		comparerow += "					<div class=\"col-md-2 col-md-offset-1\">"
				+ data.transTargetName
		"<\/div>";
		comparerow += rend_values(data);
		comparerow += "				<\/div>";
		return comparerow;
	}
	var rend_compare_result = function(data) {
		var comparerow = "";
		comparerow += "				<div class=\"row\">";
		comparerow += "					<div class=\"col-md-2 col-md-offset-1\">变化率<\/div>";
		comparerow += "					<div class=\"col-md-1 col-md-offset-5\">";
		comparerow += "						<span class=\"text-success\">" + data.transCount
				+ "<\/span>";
		comparerow += "					<\/div>";
		comparerow += "					<div class=\"col-md-1\">";
		comparerow += "						<span class=\"text-danger\">"
				+ data.completePercent + "<\/span>";
		comparerow += "					<\/div>";
		comparerow += "					<div class=\"col-md-1\">";
		comparerow += "						<span class=\"text-success\">" + data.transValue
				+ "<\/span>";
		comparerow += "					<\/div>";
		comparerow += "					<div class=\"col-md-1\">";
		comparerow += "						<span class=\"text-danger\">" + data.giveupPercent
				+ "<\/span>";
		comparerow += "					<\/div>";
		comparerow += "				<\/div>";
		return comparerow;
	}

	return {
           rend_head : rend_head,
           rend_values : rend_values,
           rend_unexpanded : rend_unexpanded,
           rend_path : rend_path,
           rend_step : rend_step,
           rend_compare_condition : rend_compare_condition,
           rend_compare_result : rend_compare_result
	}
});
