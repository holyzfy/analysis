define(['create_table' , 'table_descriptors' , 'renders'],function(create_table , table_descriptors , renders) {

	var create_step_table = function(data) {
//		var create_table = require('create_table');
//		var table_descriptors = require('table_descriptors');
//		var renders = require('renders');
		data.unshift({"level" : 3 , "fdimenName" : "步骤" , "sdimenName" : ''});
		var functions = [renders.rend_unexpanded , renders.rend_path , renders.rend_step , renders.rend_head];
		var html = create_table.create_table(table_descriptors.step_table_descriptor , functions , data);
		return html;
	};
	
		var create_compare_step_table = function(data) {
//		var create_table = require('create_table');
//		var table_descriptors = require('table_descriptors');
//		var renders = require('renders');
		data.unshift({"level" : 3 , "fdimenName" : "步骤" , "sdimenName" : ''});
		var functions = [renders.rend_unexpanded , renders.rend_path , renders.rend_step , renders.rend_head];
		var html = create_table.create_table(table_descriptors.step_compare_table_descriptor , functions , data);
		return html;
	};
	return{
		create : create_step_table,
		create_timecompare : create_compare_step_table
	};
});
