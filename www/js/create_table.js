define(function() {
	
	//non data divs
	var node = function(classname, elementname, minid, maxid, leafs) {
		this.classname = classname;
		this.elementname = elementname;
		this.minid = minid;
		this.maxid = maxid;
		this.leafs = leafs;
		this.childnodes = new Array();
		this.beginhtml = function() {
			return '<' + this.elementname + ' class=\'' + this.classname + '\'>';
		}
		this.endhtml = function() {
			return '</' + this.elementname + '>';
		}
		this.addchild = function(childnode) {
			this.childnodes.push(childnode);
			childnode.parent = this;
		}
	};
   //create a table
	var create_table = function(table_description_tree, render_functions, data) {
		var htmlblock = '';
		var root = new node('root', 'null', 0, 9999, []);
		root.addchild(table_description_tree);
		var currentnode = root;
		// add data to node
		for ( var i = 0; i < data.length; i++) {
			currentdata = data[i];
			adddata(currentdata);
		}
		// end nodes
		while (currentnode.parent != undefined) {
			htmlblock += currentnode.endhtml();
			currentnode = currentnode.parent;
		}
		// return
		return htmlblock;
		function adddata(currentdata) {
			// weather currentdata should add to this node or its child node
			if (!(currentdata.level >= currentnode.minid && currentdata.level <= currentnode.maxid)) {
				// if the currentdata should add to parent node
				// end this node
				htmlblock += currentnode.endhtml();
				// move to parent node to add this data
				currentnode = currentnode.parent;
				adddata(currentdata);
			}
			// weather add data to a leaf to this node or a child node
			else {
				// if have leaf , try to add data to a leaf
				if (currentnode.leafs.length > 0) {
					for ( var j = 0; j < currentnode.leafs.length; j++) {

						if (currentnode.leafs[j] == currentdata.level) {
							// add to a leaf
							htmlblock += (render_functions[currentdata.level])
									(currentdata);
							return;
						}
					}
				}
				// if have
				if (currentnode.childnodes.length > 0) {
					for ( var j = 0; j < currentnode.childnodes.length; j++) {
						// add to a child node
						if (currentdata.level >= currentnode.childnodes[j].minid
								&& currentdata.level <= currentnode.childnodes[j].maxid) {
							currentnode = currentnode.childnodes[j];
							htmlblock += currentnode.beginhtml();
							adddata(currentdata);
							return;
						}
					}
				}
			}

		}
	};
	
	return {
		create_table : create_table,
		node : node
	};

});
