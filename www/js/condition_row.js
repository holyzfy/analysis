define(function() {
	require('jquery');
	var operator_options = function(operates) {
		var options = '';
		for ( var i = 0; i < operates.length; i++) {
			// e-equals
			if (operates.charAt(i) == 'e') {
				options += "<option value=\"equals\">等于<\/option>";
			}
			// c-contain
			if (operates.charAt(i) == 'c') {
				options += "<option value=\"contains\">包含<\/option>";
			}
			// l-lessthan
			if (operates.charAt(i) == 'l') {
				options += "<option value=\"lessthan\">小于<\/option>";
			}
			// g-greaterthan
			if (operates.charAt(i) == 'g') {
				options += "<option value=\"greaterthan\">大于<\/option>";
			}
			// s-startwith
			if (operates.charAt(i) == 's') {
				options += "<option value=\"startwith\">开头为<\/option>";
			}
		}
		return options;
	};
	var dimen_change = function() {
		var row = $(this.parentNode);
		var operator = row.find('.operationselector');
		var options = '';
		// if choosed sourcetype, hide areaselector and input box
		if (this.value == 'sourcetype') {
			row.find('.areaselector').hide();
			row.find('.inputbox').hide();
			row.find('.sourcetypeselector').show();
			options = operator_options('e');
		}
		// if choosed area , hide sourcetypeselector and input box
		else if (this.value == 'geo') {
			row.find('.areaselector').show();
			row.find('.inputbox').hide();
			row.find('.sourcetypeselector').hide();
			options = operator_options('e');
		}
		// else , show only input box
		else {
			row.find('.areaselector').hide();
			row.find('.inputbox').show();
			row.find('.sourcetypeselector').hide();
			// step , userid , sourceurl , geoip options = 'ecs'
			if (this.value == 'step' || this.value == 'userid'
					|| this.value == 'sourceurl' || this.value == 'geoip') {
				options = operator_options('ecs');
			}
			// date , time , transcount , completepercent , transvalue ,
			// giveuppercent options = 'egl'
			if (this.value == 'date' || this.value == 'time'
					|| this.value == 'transcount'
					|| this.value == 'completepercent'
					|| this.value == 'transvalue'
					|| this.value == 'giveuppercent') {
				options = operator_options('egl');
			}
			// searchenginename , keywords options = 'ec'
			if (this.value == 'searchenginename' || this.value == 'keywords') {
				options = operator_options('ec');
			}
		}
		$(operator[0]).empty();
		$(operator[0]).append(options);
		$(operator[0]).selectpicker('refresh');
	}

	var init_condition_row = function(row) {
		var dimenselector = row.querySelector('.dimenselector');
		dimenselector.onchange = dimen_change;
		$(row).find('select').selectpicker();
		dimenselector.selectedIndex = 1;
		$(dimenselector).selectpicker('refresh');
		dimenselector.onchange();
		row.querySelector('.inputbox').textContent = '';
		row.querySelector('.deletebtn').onclick = function() {
			$(this.parentNode).remove();
			var qie = this.parentNode.parentNode.nextElementSibling;
			if (qie.className.contains('qie')) {
				qie.remove();
			}
		}
		$(row).show();
	};

	var get_conditions = function(row) {
		var condition = {};
		condition.type = row.find('.typeselector').val();
		condition.dimen = row.find('.dimenselector').val();
		condition.operator = row.find('.operationselector').val();
		if (row.find('.sourcetypeselector').is(':visible')) {
			condition.operand = row.find('.sourcetypeselector').val();
		} else if (row.find('.areaselector').is(':visible')) {
			condition.operand = row.find('.areaselector').val();
		} else if (row.find('.inputbox').is(':visible')) {
			condition.operand = row.find('.inputbox').val();
		}
		if (condition.operand == '') {
			//alert('请输入一个有效值'); 
			//throw "invalid input value";
			return undefined;
		}
		return condition;
	};

	return {
		init_condition_row : init_condition_row,
		get_conditions : get_conditions
	};
});
