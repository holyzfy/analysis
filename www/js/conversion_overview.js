define(function(require) {
	var $ = require('jquery');
	var common = require('common');
    var moment = require('moment');
    var urlMap = require('urlmap');
    require('daterangepicker');
    require('bootstrap-switch');
    var paging = require('pagination');
    var basechange = require('basechange');
	var conditionrow = require('condition_row');
	var conditionform = require('condition_form');
	require('highcharts');
	var dateJs=require('date');
	var linechart = require('conversion_overview_linechart');

    var toggleFilter = function() {
        $('#btn-filter').click(function() {
            var $btn = $(this);
            $('#conversion-filter').css('overflow', 'hidden')
                .toggleClass('open')
                .on('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function() {
                    $(this).css('overflow', '');
                });
        });
    };

    var daterange = function() {
        var $chkRange = $('#conversion-chk-range input');
        $chkRange.attr('checked',false);
        $chkRange.change(function() {
            var checked = $(this).is(':checked');
            $('#daterange2').toggle(checked);
            if(checked) daterange2.click();
        });
    };

    // 决策树分析
     var table = function() {
        var $table = $('#conversion-table');

        var init = function() {
            var switchOptions = {
                onText: '展开',
                offText: '收起',
                onInit: function(event, state) {
                    $(this).trigger('switchChange.bootstrapSwitch', [state]);
                },
                onSwitchChange: function(event, state) {
                    $(this).closest('div.item-bd').toggleClass('table-grid-expand', state);
                }
            };
            $table.find('input.conversion-switch').bootstrapSwitch(switchOptions);
        };

        // TODO 每次重新生成表格后，请执行init
        init();
    };
    
    var initConditionRow = function(conditionrow) {
		var rows = $('.condition-row');
		var row = rows[0];
		conditionrow.init_condition_row(row);
	};

	var initConditionForm = function(conditionform) {
		$('.adddimen')[0].onclick = function() {
			conditionform.addConditionRow($('.addbuttondiv'));
		};
		$('#save-conditions').click(function(e){
			e.preventDefault();
			if(conditionform.checkInput()==false){
				alert('输入信息不完整!');
				return;
			}
			$('#btn-filter').click();
			reloadTable();
		});
		$('#secDimen').change(function(){
    		reloadTable();
		});
		$('.device').change(function(){
    		reloadTable();
		});
		$('.conversion-export').click(
			function(){
				var param = conditionform.saveCondition();
		    	param.pageNum = 1;
		    	var $chkRange = $('#conversion-chk-range input');
		    	var checked = $chkRange.is(':checked');
		    	if(checked != false){
		    		param.sBeginTime= Date.parse($("#daterange2 em").text());
		    		var filterConds =  encodeURIComponent(param.filterConds);
		    		var url='excel/trans/tableTime?secDimen='+param.secDimen+
		    		'&pageNum='+param.pageNum+'&sBeginTime='+param.sBeginTime+
		    		'&filterConds='+filterConds;
		    		window.open(url);
		    	}
		    	else{
		    		var filterConds =  encodeURIComponent(param.filterConds);
		    		var url='excel/trans/table?secDimen='+param.secDimen+
		    		'&pageNum='+param.pageNum+'&filterConds='+filterConds;
		    		window.open(url);
		    	}
			}
		);
	}
    var addPaging = function(paging)
    {
    	paging.addPagination($('.pagination') , 100 ,  function(index){alert('load im ' + index);})
    	//paging.enableClick(document.getElementsByClassName('pagination')[0] , function(index){alert(index);});
    }
    
    
    var reloadTable = function(timechange){
    	if(timechange == true){
    		linechart.reloadData();
    	}
    	var param = conditionform.saveCondition();
    	param.pageNum = 1;
    	var $chkRange = $('#conversion-chk-range input');
    	var checked = $chkRange.is(':checked');
    	if(checked != false){
    		param.sBeginTime= Date.parse($("#daterange2 em").text());
    		common.post(urlMap['/trans/tableTime'], param, function(data) {
    			if (data.status === 1) {
    				var reldata = data.data;
    				
    			} else {
    				var message = data.message || '系统异常';
    				alert(message);
    			}
    		});
    	}
    	else{
    		common.post(urlMap['/trans/table'], param, function(data) {
    			if (data.status === 1) {
    				var reldata = data.data;
    				
    			} else {
    				var message = data.message || '系统异常';
    				alert(message);
    			}
    		});
    		
    	}
    }

	var start = function() {
        $(function() {
            toggleFilter();
            daterange();
            table();
            addPaging(paging);
			initConditionRow(conditionrow);
			initConditionForm(conditionform);
            common.loaded();
            basechange.start();
            dateJs.start(reloadTable);
            linechart.start();
            reloadTable();
        });
    };

    return {
        start: start
    };

});