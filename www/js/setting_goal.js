define(function(require) {
    var $ = require('jquery');
    var common = require('common');
    var urlMap = require("urlmap");
    require('bootstrap-switch');
    var basechange = require('basechange');
    var targetlist;
    var lefttargetnum = 0;
    var totaltargetnum = 10;
    var addpath = function(){
    	if(lefttargetnum <= 0){
    		alert("转化目标数量已满，请删除部分后再添加");
    	}else{
    		window.location="setting_goal_form";
    	}
        
    }

    var gettargets = function(){

        var params = {
            transid : 3
        };
        common.get(urlMap['/transtarlist'],params,function(result){
            var status = result.status;
            var jsonArray = result.data.jsonArray;
            
            lefttargetnum = totaltargetnum-jsonArray.length;
            $("#lefttargetnum").html(lefttargetnum);
            if(jsonArray != null)
            {
                targetlist = jsonArray;
                var temp1 = "";
                
                for(var i = 0;i<jsonArray.length;i++)
                {

                    temp1 += '<div class="item-bd" id="target'+jsonArray[i].tranId+'"><div class="row"><div class="col-md-1">'+(i+1)+'</div>';
                    temp1 += '<div class="col-md-1">'+jsonArray[i].targetname+'</div>';
                    temp1 += '<div class="col-md-2 text-right"><input type="checkbox" class="setting-switch"></div>';
                    temp1 += '<div class="col-md-2 item-url">'+jsonArray[i].targeturl+'</div>';
                    temp1 += '<div class="col-md-1"></div>';
                    temp1 += '<div class="col-md-2"></div>';
                    if(jsonArray[i].transtype == 0){
                    	temp1 += '<div class="col-md-1">'+'交易类型'+'</div>';
                    }else{
                    	temp1 += '<div class="col-md-1">'+'操作类型'+'</div>';
                    }
                    
                    temp1 += '<div class="col-md-2">';
                    temp1 += '<button class="btn btn-link" type="button" title="编辑" onclick="goal.vim('+jsonArray[i].tranId+')">';
                    temp1 += '<span class="glyphicon glyphicon-pencil"></span></button>';
                    temp1 += '<button class="btn btn-link" type="button" title="删除" onclick="goal.del('+jsonArray[i].tranId+')">';
                    temp1 += '<span class="glyphicon glyphicon-remove"></span></button></div></div>';
                    
                    var path = jsonArray[i].li;
                    if(path != null){
                        temp1 += '<div class="table-grid-item">';
                        for (var x = 0; x < path.length; x++) 
                        {

                            var step = eval(path[x].pathstring);
                            var giveuppoint = path[x].giveuppoint;
                            for (var y = 0; y < step.length; y++) 
                            {
                            	
                                temp1 += '<div class="row">';
                                temp1 += '<div class="col-md-offset-2 col-md-2">';
                                temp1 += '<ol class="breadcrumb" style="margin-bottom:5px;">';
                                temp1 += '<li>'+path[x].pathName+'</li>';
                                temp1 += '<li> 第'+(y+1)+'步</li>';
                                temp1 += '<li> '+step[y].stepName+'</li></ol>';
                                temp1 += '</div>';
//                              temp1 += '<div class="col-md-offset-2 col-md-2">'+path[x].pathName+' / '+step[y].stepName+'</div>';
                                temp1 += '<div class="col-md-2 item-url">'+step[y].url+'</div>';
                                if((y+1) == giveuppoint){
                                	temp1 += '<div class="col-md-1">是</div>';
                                }else{
                                	temp1 += '<div class="col-md-1">否</div>';
                                }
                                if(path[x].skip == 1){
                                	temp1 += '<div class="col-md-2">是</div></div>';
                                }else{
                                	temp1 += '<div class="col-md-2">否</div></div>';
                                }
                               
                                
                                
                            };
                        };
                        temp1+= '</div>';
                    }else{

                    }
                    temp1 += '</div>';
                }
                $("#table-grid").append(temp1);
                renderTable();
            }

            
            // $(".container-fluid table-grid").innerHTML = temp1;
        });

    }
    $("#closeposter").click(function(){
    	var parent = document.getElementById("main");
    	var deletemp = document.getElementById("poster");
    	parent.removeChild(deletemp);
    });
    var vim = function(tranId){
        
//        var params = {
//        		tranId : tranId
//        }
        window.location="/change_goal_form?tranId="+tranId;
    }

    var del = function(index){
    	if (!window.confirm("删除是不可恢复的，你确认要删除吗？")){
//    		alert("false");
    		return;
    	};
//    	alert("ss");
        var id = "target"+index;
        var deletemp = document.getElementById(id);
        var parent = document.getElementById("table-grid");
        
        var params = {
        		tranId : index
        }
        common.post(urlMap['/deleteTranDetail'],params,function(result){
//        	alert(result);
        	var status = result.status;
        	if(status == 1){
        		parent.removeChild(deletemp);
        		lefttargetnum++;
        		$("#lefttargetnum").html(lefttargetnum);
        		var targetindextag = parent.getElementsByClassName("item-bd");
        		for(var i = 0;i<targetindextag.length;i++){
        			var targetindex = targetindextag[i].getElementsByClassName("col-md-1")[0];
        			targetindex.innerHTML = i+1;
        		}
        	}
        });
        
    }
    
    var renderTable = function() {
        var $table = $('#table-grid');

        if(!renderTable.switchOptions) {
            renderTable.switchOptions = {
                onText: '展开',
                offText: '收起',
                onInit: function(event, state) {
                    $(this).trigger('switchChange.bootstrapSwitch', [state]);
                },
                onSwitchChange: function(event, state) {
                    $(this).closest('div.item-bd').toggleClass('table-grid-expand', state);
                }
            };
        }
        $table.find('input.setting-switch').bootstrapSwitch(renderTable.switchOptions);
    };


    var start = function() {
        $(function() {
            gettargets();
            basechange.start();
            // gettargets();
        });
    };

    return {
        start: start,
        vim : vim,
        del : del,
        addpath : addpath
    };

});