define(function(require) {
	var $ = require('jquery');
	var common = require('common');
	var moment = require('moment');
	var urlMap = require('urlmap');
	require('daterangepicker');
	var reloadfun = undefined;

	var daterange = function() {
		//var $chkRange = $('#conversion-chk-range input');
		var dateformat = 'YYYY/MM/DD';
		var now = moment();
		var today = now.format(dateformat);
		var yesterday = moment().subtract(1, 'd');
		var last7days = moment().subtract(7, 'd');
		var lastMonth = moment().subtract(1, 'M');
		
		var options = {
			"showDropdowns" : true,
			"ranges" : {
//				"今天" : [ now, now ],
				"昨天" : [ yesterday, yesterday ],
				"最近7天" : [ last7days, now ],
				"最新30天" : [ lastMonth, now ]
			},
			"locale" : {
				"format" : "YYYY-MM-DD",
				"separator" : " - ",
				"applyLabel" : "确定",
				"cancelLabel" : "取消",
				"fromLabel" : "从",
				"toLabel" : "到",
				"customRangeLabel" : "自定义",
				"daysOfWeek" : [ "日", "一", "二", "三", "四", "五", "六" ],
				"monthNames" : [ "1月", "2月", "3月", "4月", "5月", "6月", "7月",
						"8月", "9月", "10月", "11月", "12月" ],
				"firstDay" : 1
			},
			"startDate" : yesterday,
			"minDate" : moment().subtract(5, 'y'),
			"maxDate" : yesterday,
			"opens" : "right",
			"singleDatePicker": true
		};
		var callback = function(start, end, label) {
			var range;
			if (start.isSame(end, 'day')) {
				range = start.format(dateformat);
			} else {
				range = start.format(dateformat) + ' - '
						+ end.format(dateformat);
			}
			$('#fBeginTime').val(start._d.getTime());
			$('#fEndTime').val(end._d.getTime());
			$("#daterange em").text(range);
			var params={
					beginTime:	start._d.getTime(),
					endTime: end._d.getTime()
			};
			common.get(urlMap['/timeChange'], params, function(data) {
				if (data.status === 1) {
					if(reloadfun != undefined){
						reloadfun(true);
					}
					else{
						location.reload();
					}
				} else {
					var message = data.message || '系统异常';
					alert(message);
				}
			});
			
		};
		var callback1 = function(start, end, label) {
			var range = start.format(dateformat);
			$("#daterange2 em").text(range);
			if(reloadfun != undefined){
				reloadfun(true);
			}
			
		};

		var fDate;
		var fBeginTime = parseInt($('#fBeginTime').val());
		var fEndTime = parseInt($('#fEndTime').val());

		var fRange = [];
		if (!isNaN(fBeginTime)) {
			var begin = moment(fBeginTime).format(dateformat);
			fRange.push(begin);
			!isNaN(fEndTime)
					&& fRange.push(moment(fEndTime).format(dateformat));
			fDate = fRange.join('-');
		} else {
			fDate = today;
		}

		var options1 = $.extend({}, options, {
			startDate : fRange[0],
			endDate : fRange[1] || fRange[0],
			singleDatePicker: false
		});
		$('#daterange em').text(fDate);
		var $daterange = $('#daterange').daterangepicker(
				options1, callback);
		$daterange.on('show.daterangepicker',
				function() {
					$(this).data('daterangepicker').container
							.addClass('show-calendar');
				});
		// 比较范围
		var today = yesterday.format(dateformat);
		$('#daterange2 em').text(today);
        var $daterange2 = $('#daterange2').daterangepicker(options, callback1);
        $daterange2.on('show.daterangepicker', function() {
            $(this).data('daterangepicker').container.addClass('show-calendar');
        });
        if($('#conversion-chk-range')!=undefined){
        	
        }
	};

	var start = function(dtch) {
		reloadfun = dtch;
		daterange();
	};

	return {
		start : start
	};
});