define(function(require) {
	var $ = require('jquery');
	var common = require('common');
	var moment = require('moment');
	var urlMap = require('urlmap');
	require('daterangepicker');


	var start = function(dtch) {
		
		$('#siteselectpicker').change(function(){
			var params={
					siteId:	$('#siteselectpicker').val()
			}; 
			common.get(urlMap['/siteIdChange'], params, function(data) {
				if (data.status === 1) {
					//if(dtch != undefined){
					//	reloadfun();
					//}
					//else{
						 location.reload();
					//}
				} else {
					var message = data.message || '系统异常';
					alert(message);
				}
			});
		});
		
		$('#transselectpicker').change(function(){
			var params={
					transId:	$('#transselectpicker').val()
			}; 
			common.get(urlMap['/transTargetChange'], params, function(data) {
				if (data.status == 1) {
					if(dtch != undefined){
						dtch();
					}
					else{
						 location.reload();
					}
				} else {
					var message = data.message || '系统异常';
					alert(message);
				}
			});
		});
		
		$('#dayshowselectpicker button').click(function(){
			var params={
					timeType:	$(this).val()
			}; 
			$('#dayshowselectpicker button').removeClass('active');
			$(this).addClass('active');
			common.get(urlMap['/timeTypeChange'], params, function(data) {
				if (data.status === 1) {
					if(dtch != undefined){
						dtch();
					}
					else{
						 location.reload();
					}
				} else {
					var message = data.message || '系统异常';
					alert(message);
				}
			});
		});
		
		
		
	};

	return {
		start : start
	};
});