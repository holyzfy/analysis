define(function() {

	// get current page index
	var getCurrentPage = function(list) {
		for ( var i = 0; i < list.length; i++) {
			if (list[i].className == 'active') {
				return parseInt(list[i].textContent);
			}
		}
	};
    //returns a position in list of a labled element
	var getIndex = function(currentIndex)
	{
		return currentIndex * 2 + 1;
	}
	
	// when a page index is clicked
	var enableClick = function(pagination, callback) {
		var list = pagination.childNodes;
		for ( var i = 0; i < list.length; i++) {
			list[i].onclick = function() {
				list = this.parentNode.childNodes;
				var currentIndex = getCurrentPage(list);
				var num = this.textContent;
				var currentPage = 0;
				// <
				if (num == '<') {
					if (currentIndex != 1) {
						list[getIndex(currentIndex)].removeAttribute('class');
						list[getIndex(currentIndex - 1)].className = 'active';
						currentPage = currentIndex - 1;
					}
				}
				// >
				else if (num == '>' && currentIndex != list.length - 4) {
					list[getIndex(currentIndex)].removeAttribute('class');
					list[getIndex(currentIndex + 1)].className = 'active';
					currentPage = currentIndex + 1;
				} else
				// number
				{
					currentPage = parseInt(num);
					list[getIndex(currentIndex)].removeAttribute('class');
					list[getIndex(currentPage)].className = 'active';
				}

				// if this is the first page , disable foreback button
				if (currentPage == 1) {
					list[1].className = 'disabled';
					list[list.length - 2].removeAttribute('class');
				}
				else
				// if this is the last page, disable next button
				if(currentPage == (list.length - 1 ) / 2 - 2)
					{
					  list[list.length - 2].className = 'disabled';
					  list[1].removeAttribute('class');
					}
					else{
						list[list.length - 2].removeAttribute('class');
						list[1].removeAttribute('class');
					}
				// if the currentPage is legal
				if (currentPage != 0) {
					callback(currentPage);
				}
			}
		}
	};

	return {
		enableClick : enableClick
	};

});
