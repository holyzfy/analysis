define(function(require) {

var $ = require('jquery');
var common = require('common');
require('bootstrap-switch');

var toggleTableRow = function() {
    var options = {
        onText: '开',
        offText: '关',
        onInit: function(event, state) {
            $(this).trigger('switchChange.bootstrapSwitch', [state]);
        },
        onSwitchChange: function(event, state) {
            $(this).closest('div.item-bd, div.item-sub').toggleClass('table-grid-expand', state);
        }
    };
    $('#browser-table input.chk-switch').bootstrapSwitch(options);
};

var start = function() {
    toggleTableRow();
};

return {
    start: start
};

});