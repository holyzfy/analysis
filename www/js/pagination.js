define(function(require) {
	var $ = require('jquery');
	require('jquery-pagination');
	var addPagination = function(container , totalPageNum , callback) {
		container.pagination({
			pages: totalPageNum,
		   onPageClick: function(pageNumber, event){callback(pageNumber);},
		   prevText: '<',
		   nextText: '>'
		});
	};
	
	return{addPagination : addPagination}
});
