define(function(require) {
	var $ = require('jquery');

	var getEnv = function() {
		var devList = [
		               '127.0.0.1',
		               'localhost'
		               ];
		var isLocal = $.inArray(location.hostname, devList) > -1;
		return isLocal ? 'development' : 'production';
	};

	var devMap = {
			//页面映射
			'conversion_overview.html':'conversion_overview.html',
			'conversion_path.html':'conversion_path.html',
			'setting_goal.html':'setting_goal.html',
			//接口映射
			'/login': 'mock/login.json',
			'/trans/transform': 'mock/trans_transform.json',
			'/trans/table': 'mock/trans_table.json',
			'/siteIdChange': 'mock/siteid_change.json',
			'/trans/path': 'mock/trans_path.json',
			'/timeChange':'mock/success.json',
			'/timeTypeChange':'mock/success.json',
			'/transTargetChange':'mock/success.json',
			'/siteIdChange':'mock/success.json',
			'/transtarlist' : 'mock/transtarlist.json',
			'/change_goal_form' : 'mock/goal_detail.json',
			'/sure_edit' : 'mock/sure_edit.json',
			'/sure_add' : 'mock/sure_add.json',
			'/trans/contrast':'/trans/contrast',
			'/trans/transform':'/trans/transform'

	}

	var productionMap = {
			//页面映射
			'conversion_overview.html':'conversion_overview',
			'conversion_path.html':'conversion_path',
			'setting_goal.html':'setting_goal',	

			//接口映射
			'/trans/transform': '/trans/transform',
			'/trans/table': '/trans/table',
			'/siteIdChange': '/siteIdChange',
			'/trans/path': '/trans/path',
			'/timeChange':'/timeChange',
			'/timeTypeChange':'/timeTypeChange',
			'/transTargetChange':'/transTargetChange',
			'/siteIdChange':'/siteIdChange',
			'/login_check' : '/login_check',
			"/setting_goal" : '/setting_goal',
			"/transtarlist" : '/trantarget/showTargetList',
			"/setting_goal_form" : "/setting_goal_form",
			'/trans/contrast':'/trans/contrast',
			'/trans/transform':'/trans/transform',
			"/tranmain" :"/trantarget/tranmain",
            "/change_goal_form" : "/trantarget/showTranDetail",
            "/sure_edit" : "/trantarget/changeTranDetail",
	        "/deleteTranDetail" : "/trantarget/deleteTranDetail",
	        "/trans/tableTime" : "/trans/tableTime",
	        "/trans/table" : "/trans/table"
	   
	};

	return getEnv() === 'development' ? devMap : productionMap;
});