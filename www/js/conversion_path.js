define(function(require) {
	var $ = require('jquery');
	var common = require('common');
	var moment = require('moment');
	var urlMap = require('urlmap');
	var basechange = require('basechange');
	require('highcharts');
	var dateJs=require('date');

	function showHighcharts(data) {
		var showdata = new Array();
		var total = 0;
		for (var i = 0; i < data.paths.length; i++) {
			var temp = new Object();
			temp.name = data.paths[i].pathName;
			temp.y = data.paths[i].totalFish;
			temp.isIntermediateSum = true;
			temp.dataLabels = '比例';
			total +=temp.y;
			showdata[i] = temp;
		}
		if(total > 0){
			var ti = 0;
			for (var i = 0; i < showdata.length-1; i++) {
				showdata[i].y = (showdata[i].y*100/total);
				showdata[i].y = parseInt(showdata[i].y);
				ti += showdata[i].y;
			}
			showdata[showdata.length-1].sliced=true;
			showdata[showdata.length-1].y=100-ti;
			showPath(data.paths[data.paths.length-1]);
		}
		else{
			$(".conversion-path-main").html('');
			$("#urlin").html('');
			$("#urlout").html('');
		}
		
		
		$('#container').highcharts(
				{
					chart : {
						type : 'pie'
					},
					plotOptions : {
						pie : {
							allowPointSelect : true,
							shadow : false,
							innerSize : '60%',
							colors : [ '#50E3C2', '#FF9F00', '#8085e9',
									'#f15c80', '#e4d354', '#8085e8', '#8d4653',
									'#7cb5ec', '#434348', '#91e8e1' ],
							point : {
								events : {
									select : function() {
										var path = data.paths[this.index];
										showPath(path);
//										var ret = [ this.name, this.y,
//												this.color ].join(', ');
//										$('#current_item em').text(ret).css(
//												'color', this.color);
										
									}
								}
							}
						}
					},
					title : {
						text : data.transName,
						verticalAlign : 'middle',
						floating : true,
						style : {
							fontSize : 26
						}
					},

					tooltip : {
						valueSuffix : '%'
					},
					series : [ {
						name : '占比',
						data : showdata
					} ],
					
					credits : {
						enabled : false
					}
				});
		
	};
	function makeStep(step,blast){
       var html = "";
       if(blast){
    	   html = " <div class=\"conversion-path-label conversion-path-label-last\">目标</div>";
       }
       else{
    	   html = "<div class=\"conversion-path-label\">" +
      		"步骤"+step.transStepId+"</div>"; 
       }
       html+=
       		"<div class=\"panel\"><div class=\"panel-heading\"><h3 class=\"panel-title\">" +
       		step.transStepName +
       		"</h3></div><div class=\"panel-body\"><table><tr><td>进入次数：</td><td>" +
       		step.complete +
       		"</td></tr><tr><td>进入率：</td><td>" +
       		step.completePercent +
       		"</td></tr></table></div></div>";
       return html;
	}
	function showPath(path){
		$(".conversion-path-title").text(path.pathName);

		var htmlstep = "";
		for(var i = 0; i < path.steps.length;i++){
			htmlstep +=makeStep(path.steps[i],i == path.steps.length-1);
		}
		$(".conversion-path-main").html(htmlstep);
		var htmlurlin = "";
		var inurl = path.inUrls;
		for(var i = 0; i < inurl.length;i++){
			htmlurlin += "<tr><td><div class=\"item-url\">" +
			inurl[i].url +
					"</div></td><td>" +
					inurl[i].sv +
					"</td></tr>";
		}
		if(htmlurlout != ""){
			$('#urlin').html(htmlurlin);
		}
		else{
			$('#urlin').html("<tr><td colspan=\"2\"><div class=\"nothing\">暂无数据</div></td></tr>");
		}
		
		var htmlurlout = "";
		var outurl = path.outUrls;
		for(var i = 0; i < outurl.length;i++){
			htmlurlout += "<tr><td><div class=\"item-url\">" +
			outurl[i].url +
					"</div></td><td>" +
					outurl[i].sv +
					"</td></tr>";
		}
		if(htmlurlout != ""){
			$('#urlout').html(htmlurlout);
		}
		else{
			$('#urlout').html("<tr><td colspan=\"2\"><div class=\"nothing\">暂无数据</div></td></tr>");
		}
		
		
	};

	function loadData() {
		
		if($('#selectqy').val() != '2'){
			$('#selectsf').selectpicker('hide');
		}
		else{
			$('#selectsf').selectpicker('show');
		}
		
		var oper = new Array();
		var opId = 0;
		if($('#selectqd').val() > 0){
			oper[opId] = new Object();
			oper[opId].type='inc';
			oper[opId].dimen='sourcetype';
			oper[opId].operator='equals';
			oper[opId].operand=$('#selectqd').val();
			opId++;
		}
		if($('#selectqy').val() > 0){
			oper[opId] = new Object();
			oper[opId].type='inc';
			oper[opId].dimen='geo';
			oper[opId].operator='equals';
			if($('#selectqy').val() == 2)
			{
				oper[opId].operand=$('#selectsf').val();
			}
			else{
				oper[opId].dimen='nation';
				oper[opId].operand=900000;
				if($('#selectqy').val() == 3){
					oper[opId].type='exclu';
				}
			}
			
			opId++;
		}
		
		if($('#selectsb').val() > 0){
			oper[opId] = new Object();
			oper[opId].type='inc';
			oper[opId].dimen='dt';
			oper[opId].operator='equals';
			oper[opId].operand=$('#selectsb').val();
			opId++;
		}
		
		var params = {
				filterConds : JSON.stringify(oper)
		};

		common.post(urlMap['/trans/path'], params, function(data) {
			if (data.status === 1) {
				var reldata = data.data;
				showHighcharts(reldata);
			} else {
				var message = data.message || '系统异常';
				alert(message);
			}
		});
	};

	function event(){
		
		$('.form-horizontal .selectpicker').change(function(){

			loadData();
		});
		
		
	}
	
	var start = function() {
		$(function() {
			//daterange();
			dateJs.start(loadData);
			basechange.start(loadData);
			loadData();
			event();
		});
	};

	return {
		start : start
	};

});
