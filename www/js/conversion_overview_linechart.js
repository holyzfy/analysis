define(function(require) {
	var $ = require('jquery');
	var common = require('common');
	require('highcharts');
	var urlMap = require("urlmap");
	var dateJs=require('date');

	var lineData;
	var norm;

	var lineChart = function(id,categ,data1,data2){
		var valueSuffixStr = "";
		if(norm==selectStr.completepercent||norm==selectStr.giveuppercent){
			valueSuffixStr = "%";
		}
		$('#'+id).highcharts({
			title: {
				text: '',
				x: -20 //center
			},
			credits: {//隐藏掉highcharts的官网链接
				enabled: false
			} ,
			subtitle: {
				text: '',
				x: -20
			},
			xAxis:{
				gridLineWidth:1,
				title: {
					enabled: true,
					text: ''
				},
				maxPadding: 0.05,
				showLastLabel: true,
				categories:categ
			},
			yAxis: {
				min:0,
				title: {
					text: ''
				},
				plotLines : [{
					value : 0,
					width : 1,
					color : '#808080'
				}]
			},
			legend: {
				layout: 'vertical',
				align: 'right',
				verticalAlign: 'bottom',
				borderWidth: 0
			},
			series: [{
				name: normStr.norm1,
				data: data1
			}, {
				name: normStr.norm2,
				data: data2
			}]
		});
	}

	var normObj = {
			norm1:"trancount",
			norm2:"trancount"
	}

	var normStr = {
			norm1:"转化次数",
			norm2:"转化次数"
	}

	var selectStr = {
			trancount:"trancount",
			completepercent:"completepercent",
			transvalue:"transvalue",
			giveuppercent:"giveuppercent"
	}

	//将返回的数据转化成折线图需要的格式
	var showLineChart = function(data){
		var lineData1 = new Array();
		var lineData2 = new Array();
		var categ = new Array();
		for (var v = 0; v < data.length; v++) {
			if(normObj.norm1==selectStr.trancount){
				lineData1[v] = data[v].transCount;
			}else if(normObj.norm1==selectStr.completepercent){
				lineData1[v] = data[v].completePercent;
			}else if(normObj.norm1==selectStr.transvalue){
				lineData1[v] = data[v].transValue;
			}else if(normObj.norm1==selectStr.giveuppercent){
				lineData1[v] = data[v].giveupPercent;
			}
//			if(data[v].cate==null){
//				var date = new Date(data[v].date);
//				categ[v] = date.getFullYear()+"/"+(date.getMonth()+1)+"/"+date.getDate();
//			}else{
				categ[v] = data[v].cate;
//			}
		}
		for (var v = 0; v < data.length; v++) {
			if(normObj.norm2==selectStr.trancount){
				lineData2[v] = data[v].transCount;
			}else if(normObj.norm2==selectStr.completepercent){
				lineData2[v] = data[v].completePercent;
			}else if(normObj.norm2==selectStr.transvalue){
				lineData2[v] = data[v].transValue;
			}else if(normObj.norm2==selectStr.giveuppercent){
				lineData2[v] = data[v].giveupPercent;
			}
		}
		lineChart("conversion_overview_linechart", categ, lineData1, lineData2);
	}

	//根据用户选择的第一个对比指标的不同，第二个对比指标显示不同的下拉选项
	var selectContrast = function(norm){
		if(norm==selectStr.trancount||norm==selectStr.transvalue){
			var html = "<option class=\"trancount\" selected=\"selected\">转化次数</option><option class=\"transvalue\">转化价值</option>";
			$("#conversion-overview-select2").html(html);
			$('#conversion-overview-select2').selectpicker('refresh');
			$(".conversion-overview-select:eq(3) button span:eq(0)").text("转化次数");
		}else{
			var html = " <option class=\"completepercent\" selected=\"selected\">转化率</option><option class=\"giveuppercent\">放弃率</option>";
			$("#conversion-overview-select2").html(html);
			$('#conversion-overview-select2').selectpicker('refresh');
			$(".conversion-overview-select:eq(3) button span:eq(0)").text("转化率");

		}
	}

	/*
	 * 下拉列表选择的响应对象
	 * 1.根据用户选择，第二个对比指标显示不同选项
	 * 2.将新数据显示到折线图
	 */
	var selectChange = function(){
		$(".conversion-overview-select").change(function(){
			norm = $(this).children('option:selected').attr("class");
			var str = $(this).children('option:selected').text();
			if(norm.indexOf("1")>0){
				normObj.norm1 = norm.split("1")[0];
				normStr.norm1 = str;
				normObj.norm2 = normObj.norm1;
				normStr.norm2 = str;
				selectContrast(normObj.norm1);
			}else{
				normObj.norm2 = norm;
				normStr.norm2 = str;
			}
			showLineChart(lineData);
		});
	}

	//请求数据
	var loadData = function(){
		var params = {};
		common.get(urlMap['/trans/contrast'], params, function(data) {
			if (data.status === 1) {
				lineData = data.data;
				showLineChart(lineData);
			} else {
				var message = data.message || '系统异常';
				alert(message);
			}
		});
	}

	//请求数据
	var loadSumData = function(){
		var params = {};
		common.get(urlMap['/trans/transform'], params, function(data) {
			if (data.status === 1) {
				var sumData = data.data;
				showSumData(sumData[0]);
			} else {
				var message = data.message || '系统异常';
				alert(message);
			}
		});
	}

	//显示转化概览汇总数据
	var showSumData = function(sumData){
		$('.text-primary:eq(0)').text(sumData.transCount);
		$('.text-primary:eq(1)').text(sumData.completePercent+"%");
		$('.text-primary:eq(2)').text(sumData.transValue);
		$('.text-primary:eq(3)').text(sumData.giveupPercent==null?"0%":sumData.giveupPercent+"%");
	}
	
	var reloadData = function(){
		loadData();
		loadSumData();
	}

	var start = function() {
		selectChange();
		loadData();
		loadSumData();
	};

	return {
		start : start,
		reloadData : reloadData
	};

});