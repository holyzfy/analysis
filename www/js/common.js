define(function(require) {
	var $ = require('jquery');
	var template = require('template');
    require('bootstrap');
    require('bootstrap-select');
    var urlMap = require('urlmap');

    var toggleNav = function() {
        $('#menu-toggle-bd').click(function() {
            var navExpand = !!localStorage.getItem('navExpand');
            if(navExpand) {
                $('body').removeClass('nav-expand');
                localStorage.removeItem('navExpand');
            } else {
                $('body').addClass('nav-expand');
                localStorage.setItem('navExpand', true);
            }
        });

        $('#menu ul.menu-bd > li').click(function(e) {
            e.preventDefault();
            var isDropdown = $(this).hasClass('item-dropdown');
            if(!isDropdown && $(this).hasClass('item-selected')) {
                return;
            }
            $(this).siblings().removeClass('item-active').end().toggleClass('item-active');
        });

        var navExpand = !!localStorage.getItem('navExpand');
        $('body').toggleClass('nav-expand', navExpand);
        $('#menu').show();
    };

    var renderSelect = function() {
        $('select').selectpicker();
    };

	var start = function() {
        $(function() {
            toggleNav();
            renderSelect();
        });
    };

    var loaded = function() {
        $('body').append('<div id="loaded"></div>');
    };

    var get = function() {
        var args = [].slice.call(arguments);
        var promise = $.getJSON.apply($, args);
        promise.fail(fail);
        return promise;
    };

    var post = function() {
        var args = [].slice.call(arguments);
        args = args.concat('json');
        var promise = $.post.apply($, args);
        promise.fail(fail);
        return promise;
    };

    var fail = function(err) {
        var message;
        if(err.status && err.status !== 200) {
            message = err.responseText;
        } else if(err instanceof Error) {
            message = err.message;
        } else {
            message = err;
        }
        console.error('系统异常', message);
        return message;
    };
    
    (function(){
    	$('.menu-bd li a').click(function(){
    		var hrelf= $(this).attr('href');
    		hrelf = urlMap[hrelf];
    		if(hrelf != undefined){
    			window.location.href = hrelf;
    		}
    		else{
    			//alert('此功能研发中!');
    		}
    		
    	});
    })();

	return {
        _debug: {
            fail: fail
        },
        get: get,
        post: post,
        start: start,
        loaded: loaded
    };
});