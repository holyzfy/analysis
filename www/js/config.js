require.config({
	waitSeconds: 0,
	baseUrl: 'js',
	paths: {
		'jquery': 'lib/jquery',
		'template': 'lib/template',
		'text': 'lib/text',
		'templates': '../templates',
		'bootstrap': 'lib/bootstrap',
		'moment': 'lib/moment',
		'daterangepicker': 'lib/daterangepicker',
		'bootstrap-select': 'lib/bootstrap-select',
		'bootstrap-switch': 'lib/bootstrap-switch',
		'highcharts': 'lib/highcharts',
		'jquery.validate': 'lib/jquery.validate',
		'jquery-pagination' : 'lib/jquery.simplePagination'
	},
	shim: {
		'bootstrap': ['jquery'],
		'daterangepicker': ['bootstrap', 'moment'],
		'bootstrap-select': ['jquery'],
		'bootstrap-switch': ['jquery'],
		'jquery.validate': ['jquery'],
		'jquery.validate_common': ['jquery.validate']
	}
});