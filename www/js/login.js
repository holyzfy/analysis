define(function(require){
    var $ = require('jquery');
    var common = require('common');
    var urlMap = require('urlmap');
    require('jquery.validate_common');

    var form = function() {
        var busy = false;

        var $form = $('#login-form');
        var options = {
            rules: {
                username: {
                    required: true
                },
                password: {
                    required: true
                },
                checkcode: {
                    required: true
                }
            },
            messages: {
                username: {
                    required: '请输入用户名'
                },
                password: {
                    required: '请输入密码'
                },
                checkcode: {
                    required: '请输入验证码'
                }
            }
        };

        var validator = $form.validate(options);

        $form.submit(function(e) {
            e.preventDefault();

            if(busy || !$form.valid()) return;

            busy = true;

            var params = $form.serializeArray();
            var promise = common.post(urlMap['/login_check'], params, function(data) {
                if(data.status === 1) {

                     //alert('登陆成功');
                    window.location = urlMap["/setting_goal"];
                } else {
                    var hasErr = data.data && $.isPlainObject(data.data.errors);
                    if(hasErr) {
                        validator.showErrors(data.data.errors);
                    } else {
                        alert(data.message || '系统异常');
                    }
                    randImage.src='login-image?' + new Date().getTime();
                }
            });

            promise.always(function() {
                busy = false;
            });
        });
    };
    
    var start = function() {
        form();
    }

    return {
        start: start,
    };

});