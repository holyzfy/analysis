define(['create_table'] , function(table_basics) {
	var node = table_basics.node;
	
	
	//build descriptor for step table
	var step_table_descriptor = new node('container-fluid table-grid','div',0 ,3,[3]);
	var item_bd = new node('row item-hd' , 'div' , 0 , 2 , [0]);
	var table_grid_item = new node('table-grid-item' , 'div' , 1 , 2 , [1,2]);
	item_bd.addchild(table_grid_item);
	step_table_descriptor.addchild(item_bd);
	
	
	//build descriptor for step time compare table
	var step_compare_table_descriptor= new node('container-fluid table-grid','div',0 ,5,[5]);
	var item_bd_compare = new node('row item-hd' , 'div' , 0 , 4 , [0]);
	var table_grid_item_compare = new node('table-grid-item' , 'div' , 1 , 4 , [1,2,3,4]);
	item_bd.addchild(table_grid_item_compare);
	step_compare_table_descriptor.addchild(item_bd_compare);
	
	step_table_descriptor.beginhtml = function(){return '';};
	step_table_descriptor.endhtml = function(){return '';};
	step_compare_table_descriptor.beginhtml = function(){return '';};
	step_compare_table_descriptor.endhtml = function(){return '';};
    //return descriptors
    return{
    	step_table_descriptor : step_table_descriptor,
        step_compare_table_descriptor : step_compare_table_descriptor
    };

});
