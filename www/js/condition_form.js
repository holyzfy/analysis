define(function() {
	require('jquery');

	var addConditionRow = function(buttondiv) {
		var condition_row = require('condition_row');
		var template = require('template');
		// add qie
		var qie = "";
		qie += "               <div class=\"row qie\">";
		qie += "                    <div class=\"col-md-offset-1 col-md-11\">";
		qie += "                        <div class=\"item-row-bd\">";
		qie += "                            <span class=\"badge\">且<\/span>";
		qie += "                        <\/div>";
		qie += "                    <\/div>";
		qie += "                <\/div>";

		// add line
		var conditionline = template.compile($('#condition-row').html());
		conditionline = '<div class=\"col-md-offset-1 col-md-11 condition-row\">'
				+ conditionline() + '</div>';
		buttondiv.prevUntil().append(conditionline);
		var l = buttondiv.prevUntil().find('.condition-row');
		// buttondiv.prepend(qie);
		condition_row.init_condition_row(l[l.length - 1]);
	}
	var checkInput = function(){
		var condition_row = require('condition_row');
		var conditionRows = $('.condition-row');
		var advanceConditions = new Array();
		for ( var i = 0; i < conditionRows.length; i++) {
			var condition = condition_row
			.get_conditions($(conditionRows[i]));
			if(condition == undefined){
				return false;
			}
		}
		return true;
	}
	var saveCondition = function() {
		var cond = new Object();
		var condition_row = require('condition_row');
		var conditionRows = $('.condition-row');
		var advanceConditions = new Array();
		for ( var i = 0; i < conditionRows.length; i++) {
			var condition = condition_row
			.get_conditions($(conditionRows[i]));
			if(condition != undefined){
				advanceConditions.push(condition);
			}
		}
		var secDimen = $("#secDimen").val();
		var devices = $(".device");
		var device = '';
		for ( var i = 0; i < devices.length; i++) {
			if($(devices[i]).is(':checked'))
				{
				 device = devices[i].id;
				}
		}

        var deviceCondition = {};
        deviceCondition.type = "inc";
        deviceCondition.dimen = "dt";
        deviceCondition.operator = "equals";
        deviceCondition.operand = device;
        if(device != '' && device != undefined){
        	advanceConditions.push(deviceCondition);
        }
        
        cond.secDimen = secDimen;
		cond.filterConds = JSON.stringify(advanceConditions);
        
        return cond;
	}
	return {
		addConditionRow : addConditionRow,
		saveCondition : saveCondition,
		checkInput : checkInput
	};
});
