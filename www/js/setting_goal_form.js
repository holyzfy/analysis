define(function(require) {
	var $ = require('jquery');
	var common = require('common');
	var urlMap = require('urlmap');
	var pathnum = 0;
	var tag1 = 0;

	var delepath = function(index) {
		var transpathtotal1 = document.getElementById('transpathtotal');
		var transpath1 = document.getElementById("transpath" + index);
		transpathtotal1.removeChild(transpath1);
	}

	var addstep = function(index) {
		tag1++;
		var transpath = document.getElementById("transpath" + index);
		var steptotal = transpath
				.getElementsByClassName("form-group setting-goal-step");
		var length = steptotal.length;
		var id = ("" + index) + tag1;
		var temp = "";
		temp += '<div class="form-group setting-goal-step" id="stepid' + id
				+ '"><div class="col-md-12">';
		temp += '<label class="setting-goal-label">步骤' + (length + 1)
				+ '</label>';
		temp += '<span class="form-inline"><input type="text" class="form-control" placeholder="步骤名称"></span>';
		temp += '<select class="selectpicker" data-width="auto" style="diplay:none;"><option value="0">等于</option><option value="1" checked>开头为</option><option value="2">包含</option></select>';
		temp += '<span class="form-inline"><input type="text" class="form-control item-url" placeholder="步骤页面的URL"></span>';
		temp += '<label class="btn" title="放弃判断点"><input type="radio" name="radio'
				+ index + '"></label>';
		temp += '<button class="btn btn-link" type="button" onclick="goal.delestep('
				+ index + ',' + tag1 + ')">删除步骤</button>';
		temp += '</div></div>';
		$(steptotal[length - 1]).after(temp);
		$(".selectpicker").selectpicker();
	}

	var delestep = function(index, tag1) {
		var stepid = "stepid" + index + tag1;
		var transstep1 = document.getElementById(stepid);
		var parent = transstep1.parentNode;
		parent.removeChild(transstep1);
		var length = parent
				.getElementsByClassName("form-group setting-goal-step").length;
		if (length < 1) {
			var transpathtotal1 = document.getElementById('transpathtotal');
			var transpath1 = document.getElementById("transpath" + index);
			transpathtotal1.removeChild(transpath1);
		}else{
        	var stepindextag = parent.getElementsByClassName("setting-goal-label");
        	for(var i = 0;i<stepindextag.length;i++){
        		stepindextag[i].innerHTML = "步骤"+(i+1);
        	}
        };
		
	}

	$("#creattarget")
			.click(
					function() {
						// var transtarname = $("#targetname").val
						var checkinputs = document
								.getElementsByTagName('input');
						for (var i = 0; i < checkinputs.length; i++) {
							var checkinput = checkinputs[i];
							if (checkinput.type == "text") {
								if (checkinput.value == "") {
									alert("信息填写不完整");
									return;
								}
								;
							}
						}
						;
						var transtarname = $("#targetname").val();
						var transtarurl = $("#targeturl").val();
						var transtype = $("input[name='trantype']:checked")
								.val();
						var targetmatchtype = $(
								"#targetmatchtype option:selected").val();
						var transpathtotal = document
								.getElementsByClassName("panel");
						var temp = "";
						if (transpathtotal.length > 1) {

							temp += "[";

							for (var x = 0; x < transpathtotal.length - 1; x++) {
								temp += "{";
								var tranpathnametag = transpathtotal[x]
										.getElementsByClassName("form-control item-pathname");
								var tranpathname = tranpathnametag[0].value;
								var tranpathskiptag = transpathtotal[x]
										.getElementsByClassName("checkbox-inline")[0]
										.getElementsByTagName('input');
								var tranpathskip = tranpathskiptag[0].checked ? 1
										: 0;
								temp += "'skip':" + tranpathskip + ",";
								var steptotaltag = transpathtotal[x]
										.getElementsByClassName("form-group setting-goal-step");
								temp += "'pathName':" +"'"+ tranpathname+"'" + ",";
								var inputtags = transpathtotal[x]
										.getElementsByTagName("input");
								var giveuppoint = 0;
								var temptag = 0;
								for (var i = 0; i < inputtags.length; i++) {
									var input = inputtags[i];
									var type = input.type;
									if (type == "radio") {
										temptag++;
										if (inputtags[i].checked) {
											giveuppoint = temptag;
										}
										;
									}
								}
								;
								temp += "'giveuppoint':" + giveuppoint + ",";
								temp += "'operatype':0,";
								temp += "'pathstring':["
								for (var y = 0; y < steptotaltag.length; y++) {
									var stepnametag = steptotaltag[y]
											.getElementsByClassName("form-control");
									var stepname = stepnametag[0].value;
									var stepurltag = steptotaltag[y]
											.getElementsByClassName("form-control item-url");
									var stepurl = stepurltag[0].value;
									var pathmatchtypetag = steptotaltag[y]
											.getElementsByTagName("select");
									var pathmatchtype = pathmatchtypetag[0].value;
									temp += "{'stepName':" + "'"+stepname +"'"+ ","
											+ "'type':" + pathmatchtype + ","
											+ "'url':" +"'"+ stepurl+"'"+ "},"
								}
								;
								temp += "]},";
							}
							temp += "]";
						}
//						temp = eval(temp);
						var data = {
							name : transtarname,
							matchtype : targetmatchtype,
							url : transtarurl,
							trantype : transtype,
							tranpath : temp
						}
						common.post(urlMap['/tranmain'], data,
								function(result) {

									if (result.status > 1) {
										alert("添加成功，一天后生效");
										window.location = "setting_goal";

									} else {
										alert(result.message);
									}
								});
						//ajax请求
					});

	var addpath = function() {
		pathnum++;
		var temp = "";
		temp += '<div class="panel" id="transpath' + pathnum + '">';
		temp += '<div class="panel-heading">';
		temp += '<button class="pull-right btn btn-link" type="button" onclick="goal.delepath('
				+ pathnum + ')">';
		temp += '<span class="glyphicon glyphicon-remove"></span>删除路径</button>';
		temp += '</button><div class="form-inline"><input type="text" class="form-control item-pathname" placeholder="路径名称"><div class="checkbox-inline">';
		temp += '<label><input type="checkbox"> 只有经过此路径的目标记为转化</label>';
		temp += '</div></div></div>';
		temp += '<div class="panel-body"><div class="form-group"><div class="col-md-12"><div class="setting-goal-radio-label">放弃判断点';
		temp += '</div></div></div>';

		tag1++;

		var id = ("" + pathnum) + tag1;

		temp += '<div class="form-group setting-goal-step" id="stepid' + id
				+ '"><div class="col-md-12">';
		temp += '<label class="setting-goal-label">步骤1</label>';
		temp += '<span class="form-inline"><input type="text" class="form-control" placeholder="步骤名称"></span>';
		temp += '<select class="selectpicker" data-width="auto"><option value="0">等于</option><option value="1" checked>开头为</option><option value="2">包含</option></select>';
		temp += '<span class="form-inline"><input type="text" class="form-control item-url" placeholder="步骤页面的URL"></span>';
		temp += '<label class="btn" title="放弃判断点"><input type="radio" name="radio'
				+ pathnum + '"></label>';
		temp += '<button class="btn btn-link" type="button" onclick="goal.delestep('
				+ pathnum + ',' + tag1 + ')">删除步骤</button>';
		temp += '</div></div>';

		temp += '<div class="form-group"><div class="col-md-12"><button class="btn btn-link setting-goal-add" type="button" onclick="goal.addstep('
				+ pathnum + ')">';
		temp += '<span class="glyphicon glyphicon-plus"></span>添加步骤</button></div></div>';
		temp += '</div></div>';
		$("#transpathtotal").append(temp);
		$(".selectpicker").selectpicker();
	}

	var reset = function() {
		$("input[type='text']").val("");
	}

	var start = function() {
		$(function() {

			common.start();
		});
	};

	return {
		start : start,
		delepath : delepath,
		addstep : addstep,
		delestep : delestep,
		addpath : addpath,
		reset : reset
	};

});